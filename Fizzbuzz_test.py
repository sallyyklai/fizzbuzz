import unittest
from Fizzbuzz import *

class testfunc(unittest.TestCase):
    def setUp(self):
        print("setting up")
    def tearDown(setUp):
        print()
    def test1(self):
        self.assertEqual(fizzbuzz(3),'fizz')
    def test2(self):
        self.assertEqual(fizzbuzz(1), 1)
    def test3(self):
        self.assertEqual(fizzbuzz(5),'buzz')
    def test4(self):
        self.assertEqual(fizzbuzz(15),'fizzbuzz')

if __name__=='__main__':
    unittest.main() #invokes runner on the extended class
